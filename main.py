def receive_n_digits_number(n: int) -> int:
    raw_number = input(f"Enter a {n}-digit{'' if n == 1 else 's'} long number: ")

    if len(raw_number) == n:
        return int(raw_number)
    else:
        return receive_n_digits_number(n)


def to_digit_array(number: int) -> list[int]:
    digits: list[int] = []

    while number > 0:
        digits.append(number % 10)
        number //= 10

    return digits


if __name__ == "__main__":
    NUMBER_LENGTH = 4

    user_number = receive_n_digits_number(NUMBER_LENGTH)
    print(
        f"""The number is {user_number}
        \rIts digits are {', '.join(str(user_number))}
        \rThe digits' sum {sum(to_digit_array(user_number))}"""
    )
